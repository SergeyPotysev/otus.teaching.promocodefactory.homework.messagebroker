﻿using System;

namespace MessageType
{
    public class EmployeeGuid
    {
        public Guid Id { get; set; }
    }
}