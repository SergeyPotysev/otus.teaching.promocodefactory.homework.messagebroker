﻿using System;
using System.Threading.Tasks;
using MassTransit;
using MessageType;
using Microsoft.Extensions.Logging;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Services
{
    public class GivingToCustomerConsumer : IConsumer<GivePromoCodeRequest>
    {
        private ILogger<GivingToCustomerConsumer> _logger;
        private readonly IGivePromoCodeService _givePromoCodeService;

        public GivingToCustomerConsumer(ILogger<GivingToCustomerConsumer> logger, IGivePromoCodeService givePromoCodeService)
        {
            _logger = logger;
            _givePromoCodeService = givePromoCodeService;
        }
        
        public async Task Consume(ConsumeContext<GivePromoCodeRequest> context)
        {
            // Получен запрос на выдачу промокода клиентам
            var data = context.Message;
            
            await _givePromoCodeService.GivePromoCodesToCustomersWithPreferenceAsync(data);
            
            var message = $"[{DateTime.Now}] GivingToCustomer microservice received: {data.PromoCode} - {data.ServiceInfo}";
            _logger.LogInformation(message);
            // await Console.Out.WriteLineAsync(message);
        }
    }
}