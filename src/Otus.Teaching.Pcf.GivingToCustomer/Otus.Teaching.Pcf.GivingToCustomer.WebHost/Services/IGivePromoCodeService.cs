﻿using System.Threading.Tasks;
using MessageType;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Services
{
    public interface IGivePromoCodeService
    {
        Task GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request);
    }
}