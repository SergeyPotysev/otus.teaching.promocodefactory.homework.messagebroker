﻿using System;
using System.Threading.Tasks;
using MassTransit;
using MessageType;
using Microsoft.Extensions.Logging;

namespace Otus.Teaching.Pcf.Administration.Core.Services
{
    public class AdministrationConsumer : IConsumer<EmployeeGuid>
    {
        private ILogger<AdministrationConsumer> _logger;
        private IEmployeeService _employeeService;

        public AdministrationConsumer(ILogger<AdministrationConsumer> logger, IEmployeeService employeeService)
        {
            _logger = logger;
            _employeeService = employeeService;
        }
        
        public async Task Consume(ConsumeContext<EmployeeGuid> context)
        {
            // Получен Guid сотрудника для обновления количества промокодов
            var data = context.Message;
            
            await _employeeService.UpdateAppliedPromoCodesAsync(data.Id);
            
            var message = $"[{DateTime.Now}] Administration microservice received: {data.Id}";
            _logger.LogInformation(message);
            // await Console.Out.WriteLineAsync(message);
        }
    }
}